# Demystifying systemd - 2015 Red Hat Summit

## systemd Units

```
foo.service
bar.socket
baz.device
qux.mount
waldo.automount
thud.swap
grunt.target
snork.timer
grault.path
garply.snapshot
pizza.slice
tele.scope
```

### systemd Units: httpd.service

```
[Unit]
Description=The Apache HTTP Server
After=remote-fs.target nss-lookup.target

[Service]
Type=notify
EnvironementFile=/etc/sysconfig/httpd
ExecStart=/usr/sbin/httpd $OPTIONS -DFORGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
ExecStop=/usr/sbin/https $OPTIONS -k graceful-stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

## References

* [Demystifying systemd - 2015 Red Hat Summit](https://www.youtube.com/watch?v=S9YmaNuvw5U)

* [systemd-for-admins Part 1](http://0pointer.de/blog/projects/systemd-for-admins-1.html)
* [systemd-for-admins Part 2](http://0pointer.de/blog/projects/systemd-for-admins-2.html)
* [systemd-for-admins Part 3](http://0pointer.de/blog/projects/systemd-for-admins-3.html)
* [systemd-for-admins Part 3](http://0pointer.de/blog/projects/systemd-for-admins-4.html)
